from app.config import PRODUCT_ID, TELEGRAM_CHATIDS,TELEGRAM_TOKEN, PRODUCTS_STOCK, save_to_db
from app.pricesmart_crawler import crawl
from app.telegram_bot import TelegramBot


def main(
    product_id: int,
    clubs: list[str],
    telegram_chat_id: list[str],
    telegram_token: str,
):
    
    product = crawl(product_id, clubs)
    club_stock_delta = product.club_stock_delta(PRODUCTS_STOCK)
    save_to_db(product.id, club_stock_delta)

    if club_stock_delta: #  Send message if stock changed
        lines = [product.name.strip()]
        for club_name, in_stock in club_stock_delta.items():
            lines.append(f'{"H" if in_stock else "No h"}ay en {club_name}')

        bot = TelegramBot(telegram_token)
        for id in telegram_chat_id:
            bot.send_text(id, '\n'.join(lines))


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        prog='Pricesmart Stock Checker',
        description='Checks Pricesmart stock',
        epilog='Check if a product is in stock for a particular location, works for honduras site.'
    )
    parser.add_argument('product_id', type=int, default=PRODUCT_ID)
    parser.add_argument('-c', '--club', nargs="*", default=["El Sauce", "Florencia"])
    parser.add_argument('--chat_id', nargs="+", default=TELEGRAM_CHATIDS)
    parser.add_argument('--ttoken', dest="telegram_token", default=TELEGRAM_TOKEN)
    args = parser.parse_args()
    main(
        args.product_id,
        args.club,
        args.chat_id,
        args.telegram_token,
    )
