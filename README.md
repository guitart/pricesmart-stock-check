# Pricesmart Crawler Stock Checker
Check if a product is in stock for a particular location, works for honduras site.
This should run in a cronjob.

## Quick Start
```sh
poetry install
poetry run python main.py <product_id> --chat_id <chat_id> <chat_id> --ttoken <telegram_token>
```

## Use Bac checker
Create a `.env` file with the following structure

```
BAC_USERNAME=
BAC_PASSWORD=
BAC_ACCOUNT_NUMBER=
```

Run the following command:

```sh
poetry run python test_bac.py
```