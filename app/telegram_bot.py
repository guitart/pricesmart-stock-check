import requests

from .config import TELEGRAM_TOKEN

class TelegramBot():
    def __init__(self, bot_token = ''):
        self.bot_token = bot_token or TELEGRAM_TOKEN
    
    def execute_method(self, method: str, data: dict):
        response = requests.post(f'https://api.telegram.org/bot{self.bot_token}/{method}', data=data)
        return response.json()

    def send_text(self, chat_id: str, text: str):
        return self.execute_method('sendMessage', {
            'parse_mode': 'Markdown',
            'text': text,
            'chat_id': chat_id,
        })

