from datetime import datetime
from decimal import Decimal
from typing import Optional
from urllib.parse import urlparse, ParseResult

from bs4 import BeautifulSoup
from lxml import etree

from app.bank_crawler.base import BankCrawler, Transaction
from app.date_utils import first_day_of_month, last_day_of_month
from app.config import BAC_ACCOUNT_NUMBER, BAC_USERNAME, BAC_PASSWORD

class BacCrawler(BankCrawler):
    bac_date_format = '%d/%m/%Y'

    @property
    def details_url(self):
        return f'{self.base_url}/ebac/module/transfer/ShowTransferDetailAccountState.go'

    @property
    def logout_url(self):
        return f'{self.base_url}/ebac/common/logout.go'

    @property
    def default_account_balance_url(self):
        return f'{self.base_url}/ebac/module/accountbalance/accountBalance.go'

    @property
    def query_transactions_url(self):
        return f'{self.base_url}/ebac/module/bankaccountstate/queryBankTransaction.go'

    def __init__(self, username='', password='', bank_account='') -> None:
        super().__init__()
        self.base_url = 'https://www.baccredomatic.com'
        self.session.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36'
        self.session.headers['Origin'] = self.base_url
        self.username = username or BAC_USERNAME
        self.password = password or BAC_PASSWORD
        self.bank_account = bank_account or BAC_ACCOUNT_NUMBER

    def login(self) -> None:
        login_data = {
            'bac_signature': 'contrasena',
            'product': self.username,
            'pass': self.password,
            'token': '',
            'op': 'ingresar',
            'signatureDataHash': '',
        }
        self.session.cookies.set('selectedCountry', 'https://www.baccredomatic.com/es-hn', domain='www.baccredomatic.com')
        self.session.get('https://www.baccredomatic.com/es-hn')
        res = self.session.post('https://www.sucursalelectronica.com/redir/redirect.go?country=HN', data=login_data, allow_redirects=True)
        url_parse = urlparse(res.url)
        self.base_url = ParseResult(url_parse.scheme, url_parse.netloc,'','','','').geturl()
        self.session.headers['Origin'] = self.base_url


    def get_transactions(self, month_date: Optional[datetime]=None) -> list[Transaction]:
        r = self.session.get(self.default_account_balance_url)
        soup = BeautifulSoup(r.text, 'html.parser')
        dom = etree.HTML(str(soup))
        product_id = dom.xpath(f"//select[@name='productId']/option[contains(text(),'{self.bank_account}')]/@value")[0]
        today = month_date or datetime.now()
        
        data = {
            'productId': product_id,
            'initDate': first_day_of_month(today).strftime(self.bac_date_format),
            'endDate': last_day_of_month(today).strftime(self.bac_date_format),
        }
        r = self.session.post(
            self.query_transactions_url,
            data=data
        )
        
        return self._create_transactions(r.json())

    def get_transaction_details(self, transactions: list[Transaction]) -> list[Transaction]:
        return []

    def save_transactions(self, transactions: list[Transaction]) -> None:
        pass

    def logout(self) -> None:
        self.session.get(self.logout_url)

    def _create_transactions(self, data: dict) -> list[Transaction]:
        transactions = data['bankAccountStateView']['bankAccountStateMovementView']
        return [
            self._create_transaction(tx)
            for tx in transactions
        ]

    def _create_transaction(self, tx: dict):
        created_at = datetime(
            tx['date']['year'],
            tx['date']['month']+1,
            tx['date']['dayOfMonth']
        )
        is_debit = tx['isDebit']
        return Transaction(
            amount=round(Decimal(tx['amount']), 2),
            bank_from=tx['stmacc'] if is_debit else tx['details'],
            bank_to=tx['details'] if is_debit else tx['stmacc'],
            created_at=created_at,
            reference=tx['reference'],
            tx_type='debit' if is_debit else 'credit',
        )