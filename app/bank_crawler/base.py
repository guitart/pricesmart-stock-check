import abc
from datetime import datetime
from decimal import Decimal
from typing import Literal
from pydantic import BaseModel

import requests


class Transaction(BaseModel):
    reference: str
    created_at: datetime
    bank_from: str
    bank_to: str
    amount: Decimal
    tx_type: Literal['debit', 'credit']

class BankCrawler(abc.ABC):
    @abc.abstractmethod
    def login(self) -> None:
        pass

    @abc.abstractmethod
    def get_transactions(self) -> list[Transaction]:
        pass

    @abc.abstractmethod
    def get_transaction_details(self, transactions: list[Transaction]) -> list[Transaction]:
        pass

    @abc.abstractmethod
    def save_transactions(self, transactions: list[Transaction]) -> None:
        pass

    @abc.abstractmethod
    def logout(self) -> None:
        pass

    def __init__(self) -> None:
        super().__init__()
        self.session = requests.Session()

    def __enter__(self):
        self.login()
        return self
    
    def __exit__(self, exc_type, exc_value, exc_tb):
        self.logout()

    def crawl(self):
        self.login()
        transactions = self.get_transactions()
        transactions_details =self.get_transaction_details(transactions)
        self.save_transactions(transactions_details)
        self.logout()