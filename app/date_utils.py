from datetime import datetime, timedelta

def first_day_of_month(input_datetime: datetime):
    return input_datetime.replace(day=1)

def first_day_next_month(input_datetime: datetime):
    next_month = input_datetime + timedelta(days=32)
    return first_day_of_month(next_month)

def last_day_of_month(input_datetime: datetime):
    return first_day_next_month(input_datetime) - timedelta(days=1)
