import json
import os

try:
    from dotenv import load_dotenv
    load_dotenv()
except ModuleNotFoundError:
    pass


PRODUCT_ID = os.getenv('PRODUCT_ID')
TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN')
TELEGRAM_CHATIDS = os.getenv('TELEGRAM_CHATID')
TELEGRAM_CHATIDS = TELEGRAM_CHATIDS.split(',') if TELEGRAM_CHATIDS else []

BAC_USERNAME = os.getenv('BAC_USERNAME')
BAC_PASSWORD = os.getenv('BAC_PASSWORD')
BAC_ACCOUNT_NUMBER = os.getenv('BAC_ACCOUNT_NUMBER')

db_path = './db.json'

def save_to_db(product_id, delta={}):
    if not delta:
        return

    for key, value in delta.items():
        PRODUCTS_STOCK[f'{product_id}:{key}'] = value

    with open(db_path, 'w') as f:
        json.dump(PRODUCTS_STOCK, f)

def load_db() -> dict[str, bool]:
    try:
        with open(db_path) as f:
            return json.load(f)
    except FileNotFoundError:
        return {}

PRODUCTS_STOCK = load_db()


