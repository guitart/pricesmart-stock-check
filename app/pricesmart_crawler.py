from dataclasses import dataclass
import requests
from lxml import etree
from bs4 import BeautifulSoup

@dataclass
class PricesmartProduct:
    id: int
    name: str
    clubs_in_stock: dict[str, bool]

    def club_stock_delta(self, stock: dict[str, bool]) -> dict[str, bool]:
        club_stock_delta = {}
        for club_name, in_stock in self.clubs_in_stock.items():
            stock_key = f'{self.id}:{club_name}'
            in_stock_before = stock.get(stock_key)
            if in_stock is not in_stock_before:
                club_stock_delta[club_name] = in_stock
        return club_stock_delta

def crawl(product_id, clubs):
    r = requests.get(f'https://www.pricesmart.com/site/hn/en/pdp/{product_id}')
    soup = BeautifulSoup(r.text, 'html.parser')
    dom = etree.HTML(str(soup))
    clubs_text_path = [f'text()="{c}"' for c in clubs]
    ps = dom.xpath(f'//p[span[{" or ".join(clubs_text_path)}]]')
    name: str = dom.xpath('//h1[@id="product-display-name"]')[0].text
    club_stock_now: dict[str, bool] = {}
    for p in ps:
        icon = p[0]
        club_name: str = p[1].text
        in_stock = 'fa-check' in icon.get('class')
        club_stock_now[club_name] = in_stock

    return PricesmartProduct(product_id, name, club_stock_now)

