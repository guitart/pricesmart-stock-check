from datetime import datetime
from app.bank_crawler.bac import BacCrawler

print('test started')

txs = []
with BacCrawler() as crawler:
    txs.extend(crawler.get_transactions())
    txs.extend(crawler.get_transactions(month_date=datetime(2024, 4, 1)))

for tx in txs:
    print(tx.model_dump_json(indent=2))